import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchStuff } from "./actions/testAction";
import FacebookLogin from "react-facebook-login";
import {
  fbLoginFailure,
  fbLoginRequest,
  fbLoginSuccess
} from "./actions/FbLogin";

class App extends Component {
  render() {
    const responseFacebook = async response => {
      if (response.id) {
        await this.props.fbLoginRequest();
        this.props.fbLoginSuccess(response);
      } else {
        await this.props.fbLoginRequest();
        this.props.fbLoginFailure();
      }
    };

    return (
      <div className="App">
        <FacebookLogin
          onClick={() => this.props.fbLoginRequest}
          appId="209895893005015"
          autoLoad={false}
          fields="name,email,picture"
          callback={responseFacebook}
          cssClass="my-facebook-button-class"
        />,
      </div>
    );
  }
}

export default connect(
  null,
  { fetchStuff, fbLoginSuccess, fbLoginRequest, fbLoginFailure }
)(App);
