export const FETCH_STUFF = "FETCH_STUFF";
export const RECEIVE_STUFF = "RECEIVE_STUFF";
export const FB_LOGIN_REQUEST = "FB_LOGIN_REQUEST";
export const FB_LOGIN_SUCCESS = "FB_LOGIN_SUCCESS";
export const FB_LOGIN_FAILURE = "FB_LOGIN_FAILURE";
