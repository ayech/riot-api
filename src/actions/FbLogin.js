import {
  FB_LOGIN_REQUEST,
  FB_LOGIN_SUCCESS,
  FB_LOGIN_FAILURE
} from "./actionsTypes";

export function fbLoginRequest() {
  return dispatch => {
    dispatch({ type: FB_LOGIN_REQUEST });
  };
}
export function fbLoginSuccess(data) {
  return dispatch => {
    dispatch({ type: FB_LOGIN_SUCCESS, payload: data });
  };
}
export function fbLoginFailure() {
  return dispatch => {
    dispatch({ type: FB_LOGIN_FAILURE });
  };
}
