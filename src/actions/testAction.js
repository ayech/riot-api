import { FETCH_STUFF, RECEIVE_STUFF } from "./actionsTypes";
import axios from "axios";

export function fetchStuff() {
  return dispatch => {
    dispatch({ type: FETCH_STUFF });
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(res => {
        dispatch({ type: RECEIVE_STUFF, payload: res.data });
      })
      .catch(e => {
        console.log(e);
      });
  };
}
export function recieveStuff(data) {
  return dispatch => {
    dispatch({ type: RECEIVE_STUFF, payload: data });
  };
}
