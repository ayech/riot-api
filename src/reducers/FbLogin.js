import {
  FB_LOGIN_FAILURE,
  FB_LOGIN_REQUEST,
  FB_LOGIN_SUCCESS
} from "../actions/actionsTypes";

export default function fbLogin(state = {}, action) {
  switch (action.type) {
    case FB_LOGIN_REQUEST:
      return state;
    case FB_LOGIN_SUCCESS:
      return action.payload;
    case FB_LOGIN_FAILURE:
      return state;
    default:
      return state;
  }
}
