import { combineReducers } from "redux";
import testReducer from "./testReducer";
import FbLogin from "./FbLogin";

const rootReducer = combineReducers({
  testReducer,
  FbLogin
});

export default rootReducer;
