import { FETCH_STUFF, RECEIVE_STUFF } from "../actions/actionsTypes";

export default function stuff(state = {}, action) {
  switch (action.type) {
    case FETCH_STUFF:
      return state;
    case RECEIVE_STUFF:
      return action.payload;
    default:
      return state;
  }
}
